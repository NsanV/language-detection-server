import GcpLanguageApi from "../api/gcpLanguageApi"
import { AzureLanguageApi } from "../api/azureLanguageApi"
import { IDetectResponse } from '../models/IDetect';

export default class DetectionService {

  async gcpDetection(text: string): Promise<IDetectResponse> {
    let detectLanguage = await new GcpLanguageApi().detectLanguage(text)
    try {
      detectLanguage = {
        language: detectLanguage.language,
        score: detectLanguage.confidence,
        provider: `Google`
      }
      return detectLanguage
    } catch (error) {
      return (detectLanguage = { language: ``, score: 0, provider: `` })
    }
  }

  async azureDetection(text: string): Promise<IDetectResponse> {
    let detectLanguage: any = await new AzureLanguageApi().detectLanguage({text})
    try {
      detectLanguage = {
        language: detectLanguage[0].language,
        score: detectLanguage[0].score,
        provider: `Azure`
      }
      return detectLanguage
    } catch (error) {
      console.log(error)
      return (detectLanguage = { language: ``, score: 0, provider: `` })
    }
  }

  async allDetection(text: string): Promise<IDetectResponse[]> {
    const detectionProvider = [this.gcpDetection(text), this.azureDetection(text)]
    return Promise.all(detectionProvider).then(data => {
      return data
    })
  }
}
