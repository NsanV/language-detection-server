import { expect } from "chai"
import * as sinon from "sinon"
import GcpLanguageApi from "../api/gcpLanguageApi"
import DetectionService from "./detectionService"
import { IDetectResponse } from "../models/IDetect"

describe(`detection service`, () => {
  let service: DetectionService
  beforeEach(() => {
    service = new DetectionService();
  });

  it(`Detect 'Hello world with Google' as english language with Google provider`, () => {
    sinon.stub(GcpLanguageApi.prototype, `detectLanguage`).returns(
      new Promise<IDetectResponse>(resolve =>
        resolve({
          language: `en`,
          score: 0.982610285282135,
          provider: `Google`
        })
      )
    );
    const text = `Hello world with Google`;
    // Test
    const result: Promise<IDetectResponse> = service.gcpDetection(text);
    // Assert
    const expected = {
      language: `en`,
      score: 0.982610285282135,
      provider: `Google`
    };
    result
      .then(res => {
        expect(res).equal(expected);
      })
      .catch(err => {});
  })

  it(`Detect 'Hello world with Azure' as an english language with Azure provider`, () => {
    sinon.stub().returns(
      new Promise<IDetectResponse>(resolve =>
        resolve({
          language: `en`,
          score: 1,
          provider: `Azure`
        })
      )
    );
    const text = `Hello world with Azure`;
    // Test
    const result: Promise<IDetectResponse> = service.azureDetection(text);
    // Assert
    const expected = {
      language: `en`,
      score: 1,
      provider: `Azure`
    };
    result
      .then(res => {
        expect(res).equal(expected);
      })
      .catch(err => {});
  })

  it(`Detect 'Hello world' as an english language with Azure and Google provider`, () => {
    sinon.stub().returns(
      new Promise<IDetectResponse[]>(resolve =>
        resolve([{
          language: `en`,
          score: 1,
          provider: `Google`
        }, {
          language: `en`,
          score: 1,
          provider: `Azure`
        }])
      )
    );
    const text = `Hello world`;
    // Test
    const result: Promise<IDetectResponse[]> = service.allDetection(text);
    // Assert
    const expected = [{
      language: `en`,
      score: 1,
      provider: `Google`
    }, {
      language: `en`,
      score: 1,
      provider: `Azure`
    }]
    result
      .then(res => {
        expect(res.length).equal(2)
        expect(res[0].language).equal('en')
        expect(res[0].score).equal('0.8867332935333252')
        expect(res[0].provider).equal('Google')
        expect(res[1].language).equal('en')
        expect(res[1].score).equal('1')
        expect(res[1].provider).equal('Microsoft')
      })
      .catch(err => {});
  })
})
