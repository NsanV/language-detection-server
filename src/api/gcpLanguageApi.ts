const GCPTranslate = require("@google-cloud/translate")
import { IDetectResponse } from "../models/IDetect"

export default class GcpLanguageApi {
    getLanguageApiGcp() {
        return new GCPTranslate.Translate({
            projectId: process.env.GOOGLE_CLOUD_PROJECT_ID
            /* test comment */
            // test comment
        })
    }

    async detectLanguage(text: string) {
        return await this.getLanguageApiGcp()
        .detect(text)
        .then((data: IDetectResponse[]) => {
            return data[0] 
        }).catch((e: any) => {
            console.error(e.message);
        })
    }
}