import { AzureTranslator } from "azure-translate"
import { IDetectOptions, IDetectResponse } from "../models/IDetect"

export class AzureLanguageApi {
    translator: any
    getLanguageApiAzure() {
        this.translator = new AzureTranslator(process.env.AZURE_API_KEY1!)
        return this.translator
    }

    detectLanguage(args: IDetectOptions) {
        return this.getLanguageApiAzure()
        .detect(args)
        .then((data: IDetectResponse[]) => {
            return data
        }).catch((e: any) => {
            console.error(e.message);
        })
    }

}