export interface IBodyOptions {
  text: string | string[]
  translation?: string
}

export interface IHeaders {
  "X-ClientTraceId": string
}