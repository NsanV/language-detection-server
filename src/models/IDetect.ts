import { IHeaders } from "./IShared"

export interface IDetectOptions {
  text: string | string[]
  header?: IHeaders
}

export interface IDetectResponse {
  language: string
  score: number
  provider: string
  input?:string
}



