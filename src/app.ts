import express, { Request, Response, NextFunction } from "express"
import * as bodyParser from "body-parser"
import DetectionService from "./services/detectionService"
import "dotenv/config"
import cors from 'cors'

class App {
  public express: any
  constructor() {
    this.express = express()
    this.routes()
  }

  private routes(): void {
    const router = express.Router()
    router.use(bodyParser.json())
    router.use(cors())

    router.use((req: Request, res: Response, next: NextFunction) => {
      if (this.basicAuthentification(req)) {
          return next()
      }
      res.set('WWW-Authenticate', 'Basic realm="401"')
      res.status(401).send('Authentication required.')
    })

    router.get(`/google/language/detection`, (req: Request, res: Response) => {
      const text: string = req.query.text
      let detetionService = new DetectionService()
      detetionService.gcpDetection(text).then(result => {
        res.json(result)
      })
    })

    router.get(`/azure/language/detection`, (req: Request, res: Response) => {
      const text: string = req.query.text
      const detetionService = new DetectionService()
      detetionService.azureDetection(text).then(result => {
        res.json(result)
      })
    })

    router.get(`/all/language/detection`, (req: Request, res: Response) => {
      const text: string = req.query.text
      const detetionService = new DetectionService()
      detetionService.allDetection(text).then(result => {
        res.json(result)
      })
    })

    this.express.use(`/`, router)
  }

  private basicAuthentification (req: Request) {
    const auth = {
      login: process.env.APP_LOGIN,
      password: process.env.APP_PASSWORD
    }
    const b64auth = (req.headers.authorization || '').split(' ')[1] || ''
    const [login, password] = Buffer.from(b64auth, 'base64')
      .toString()
      .split(':')
    return login && password && login === auth.login && password === auth.password
  }
  
}

export default new App().express
